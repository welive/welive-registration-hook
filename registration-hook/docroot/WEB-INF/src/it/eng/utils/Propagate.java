package it.eng.utils;

import it.eng.model.ExtModule;
import it.eng.service.ExtModuleLocalServiceUtil;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class Propagate {

	private static final int MESSAGE_TYPE_SUCCESS = 0;
	private static final int MESSAGE_TYPE_ERROR = 1;
	private static final int MESSAGE_TYPE_WARNING = 2;

	private static final int MESSAGE_REGISTRATION_SUCCEED = 0;
	private static final int MESSAGE_REGISTRATION_FAILED = 1;
	private static final int MESSAGE_USER_EXISTS = 2;	
	private static final int MESSAGE_JSON_ERROR = 3;
	private static final int MESSAGE_INVALID_SECRET = 4;
	private static final int MESSAGE_DECRYPTION_ERROR = 5;
	private static final int MESSAGE_EMAIL_NOT_SENT = 6;

	private static Propagate instance;

	private Log _log;
	private boolean isMsgCrypted;

	private String username;
	private String password;

	
	private Propagate() {
		_log = LogFactoryUtil.getLog(Propagate.class);
		isMsgCrypted = false;
		try {
			username = PrefsPropsUtil.getString("welive.username", PropertyGetter.getProperty("welive.username"));
			password = PrefsPropsUtil.getString("welive.password", PropertyGetter.getProperty("welive.password"));
		} catch (SystemException e) {
			username = PropertyGetter.getProperty("welive.username");
			password = PropertyGetter.getProperty("welive.password");
		}
	}


	public static Propagate getInstance() {
		if(instance == null) {
			instance = new Propagate();
		}
		return instance;
	}

	public String organizationPropagate(long ccOrganizationId, String name, String info, long leaderId) {
	

		Map<String, String> remoteToolsInError = new HashMap<String, String>();

		JSONObject json = JSONFactoryUtil.createJSONObject();
		json.put("ccOrganizationId", ccOrganizationId);
		json.put("name", name);
		json.put("info", info);
		json.put("leaderId", leaderId);



		List<ExtModule> extModuleList = new ArrayList<ExtModule>();
		try {
			extModuleList = ExtModuleLocalServiceUtil.getExtModuleByUpsertOrganizationPropagation(true);
//			extModuleList = ExtModuleLocalServiceUtil.getExtModules(0, ExtModuleLocalServiceUtil.getExtModulesCount());
		} catch (Exception e) {
			_log.error(e);
		}

		if(extModuleList.size() > 0) {
			_log.info("Propagate Organization registration to external modules...");
		}

		try {
			Client client = Client.create();
			// Basic Authentication
			client.addFilter(new HTTPBasicAuthFilter(username, password));
			
			WebResource webResource;
			ClientResponse clientResponse;
			JSONObject response;

			String url = "";
			String output = "";
			String message = "";
			String type = "";

			int t, m;

			for(int i = 0; i < extModuleList.size(); i++) {
				try {
					_log.info("..." + extModuleList.get(i).getModuleName());

					url = extModuleList.get(i).getUpsertOrganizationEndpoint();
					String contentType = extModuleList.get(i).getContentType();
					json.put("secret", extModuleList.get(i).getModuleSharedSecret());

					byte[] cipher = null;
					if(isMsgCrypted) {
						// Get public key of external module
						PublicKey pk = SecurityUtils.getPublicKeyByAlias(extModuleList.get(i).getModuleAlias());
						if(pk == null) {
							_log.error("Can't recover public key for alias '" + extModuleList.get(i).getModuleAlias() + "'");
							remoteToolsInError.put(extModuleList.get(i).getModuleName(), "Can't recover public key for alias '" + extModuleList.get(i).getModuleAlias() + "'");
							return "KO";
						}

						// Encrypt JSON
						cipher = SecurityUtils.encrypt(json.toString(), pk);
						if(cipher == null) {
							_log.error("Encryption error!");
							remoteToolsInError.put(extModuleList.get(i).getModuleName(), "Encryption error!");
							return "KO";
						}	
					}

					webResource = client.resource(url);
					_log.info("Sending message to " + url);
					if(isMsgCrypted) {
						_log.info("Encrypted message");
						clientResponse = webResource.accept("application/json").type(javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).post(ClientResponse.class, cipher);
					} else {
						_log.info("message: " + json.toString());
						clientResponse = webResource.accept("application/json").type(contentType).post(ClientResponse.class, json.toString());
					}

					if (clientResponse.getStatus() != 200) {
						_log.error("Failed - HTTP Error Code :" + clientResponse.getStatus());
						remoteToolsInError.put(extModuleList.get(i).getModuleName(), Utils.getErrorDescriptionByCode(clientResponse.getStatus()));
					}

					output = clientResponse.getEntity(String.class);

					if(output.substring(0, 1).compareTo("\"") == 0 && output.substring(output.length() - 1, output.length()).compareTo("\"") == 0) {
						output = output.substring(1, output.length() - 1);
					}

					output = output.replace("\\", "");

					response = JSONFactoryUtil.createJSONObject(output);
					_log.info("Response: " + response.toString());

					t = response.getInt("type");
					m = response.getInt("message");

					switch(t) {
						case MESSAGE_TYPE_SUCCESS:
							type = "SUCCESS"; break;
						case MESSAGE_TYPE_ERROR:
							type = "ERROR"; break;
						case MESSAGE_TYPE_WARNING:
							type = "WARNING"; break;
						default:
							type = "UNKNOWN";
					}

					switch(m) {
						case MESSAGE_REGISTRATION_SUCCEED:
							message = "Registration succeed"; break;
						case MESSAGE_REGISTRATION_FAILED:
							message = "Registration failed"; break;
						case MESSAGE_USER_EXISTS:
							message = "User already exists"; break;
						case MESSAGE_EMAIL_NOT_SENT:
							message = "Registration email not sent"; break;
						case MESSAGE_JSON_ERROR:
							message = "External module error in parsing JSON"; break;
						case MESSAGE_INVALID_SECRET:
							message = "Invalid secret"; break;
						case MESSAGE_DECRYPTION_ERROR:
							message = "External module error in decrypting request"; break;
						default:
							message = "Unknown";
					}

					_log.info(type + ": " + message);
				} catch(Exception e) {
    				String errorSending = "Error sending data to "+url+". "+e.getMessage();
    				_log.error(errorSending);
    				remoteToolsInError.put(extModuleList.get(i).getModuleName(), errorSending);
				}
			}
		} catch(Exception e) {
			_log.error(e);
		}

		return "OK";
	}

	
	public String userPropagate(long userId, int CC_UserID, String referredPilot, String firstName, String lastName, Boolean isMale, 
			Integer birthdayDay,Integer birthdayMonth, Integer birthdayYear, String email , boolean isDeveloper, String city, String address,
			String country, String zipCode, String[] languages, String role, String[] tags, String[] skills, String employment, boolean isCompany) {
		
		Map<String, String> remoteToolsInError = new HashMap<String, String>();
		String screenName = "";
		try {
			User user = UserLocalServiceUtil.getUser(userId);
			screenName = user.getScreenName();
		} catch (Exception e) {
			_log.error(e);
			
		}

		JSONObject json = JSONFactoryUtil.createJSONObject();
		json.put("id", userId);
		json.put("ccUserID", CC_UserID);
		json.put("referredPilot", referredPilot);
		json.put("firstName", firstName);
		json.put("lastName", lastName);
		if(isMale != null) {
			json.put("isMale", isMale);
		}
		json.put("liferayScreenName", screenName);

		if(birthdayDay != null && birthdayMonth != null && birthdayYear != null) {
			JSONObject birthDate= JSONFactoryUtil.createJSONObject();
			birthDate.put("day", birthdayDay);
			birthDate.put("month", birthdayMonth);
			birthDate.put("year", birthdayYear);
			json.put("birthDate", birthDate);
		}

		json.put("email", email);
		json.put("isDeveloper", isDeveloper);
		json.put("city", city);
		json.put("address", address);
		json.put("country", country);
		json.put("zipCode", zipCode);
		json.put("role", role);
		json.put("isCompany", isCompany);

		JSONArray jsonArrayLanguages = JSONFactoryUtil.createJSONArray();
		for (int i=0; i<languages.length; i++) {
			jsonArrayLanguages.put(languages[i]);
		}
		json.put("languages", jsonArrayLanguages);

		JSONArray jsonArrayTags = JSONFactoryUtil.createJSONArray();
		for (int i=0; i<tags.length; i++) {
			jsonArrayTags.put(tags[i]);
		}
		json.put("userTags", jsonArrayTags);

		// TODO: "userSkills" not yet available
		JSONArray jsonArraySkills = JSONFactoryUtil.createJSONArray();
		for (int i=0; i<skills.length; i++) {
			jsonArraySkills.put(skills[i]);
		}
		json.put("userSkills", jsonArraySkills);
		
		json.put("employement", employment);

		List<ExtModule> extModuleList = new ArrayList<ExtModule>();
		try {
			extModuleList = ExtModuleLocalServiceUtil.getExtModuleByRegistrationPropagation(true);
//			extModuleList = ExtModuleLocalServiceUtil.getExtModules(0, ExtModuleLocalServiceUtil.getExtModulesCount());
		} catch (Exception e) {
			_log.error(e);
		}

		if(extModuleList.size() > 0) {
			_log.info("Propagate registration to external modules...");
		}

		try {
			Client client = Client.create();
			// Basic Authentication
			client.addFilter(new HTTPBasicAuthFilter(username, password));
			
			WebResource webResource;
			ClientResponse clientResponse;
			JSONObject response;

			String url = "";
			String output = "";
			String message = "";
			String type = "";

			int t, m;

			for(int i = 0; i < extModuleList.size(); i++) {
				try {
					_log.info("..." + extModuleList.get(i).getModuleName());

					url = extModuleList.get(i).getModuleEndpoint();
					String contentType = extModuleList.get(i).getContentType();
					json.put("secret", extModuleList.get(i).getModuleSharedSecret());

					byte[] cipher = null;
					if(isMsgCrypted) {
						// Get public key of external module
						PublicKey pk = SecurityUtils.getPublicKeyByAlias(extModuleList.get(i).getModuleAlias());
						if(pk == null) {
							_log.error("Can't recover public key for alias '" + extModuleList.get(i).getModuleAlias() + "'");
							remoteToolsInError.put(extModuleList.get(i).getModuleName(), "Can't recover public key for alias '" + extModuleList.get(i).getModuleAlias() + "'");
							return "KO";
						}

						// Encrypt JSON
						cipher = SecurityUtils.encrypt(json.toString(), pk);
						if(cipher == null) {
							_log.error("Encryption error!");
							remoteToolsInError.put(extModuleList.get(i).getModuleName(), "Encryption error!");
							return "KO";
						}	
					}

					webResource = client.resource(url);
					_log.info("Sending message to " + url);
					if(isMsgCrypted) {
						_log.info("Encrypted message");
						clientResponse = webResource.accept("application/json").type(javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM).post(ClientResponse.class, cipher);
					} else {
						_log.info("message: " + json.toString());
						clientResponse = webResource.accept("application/json").type(contentType).post(ClientResponse.class, json.toString());
					}

					if (clientResponse.getStatus() != 200) {
						_log.error("Failed - HTTP Error Code :" + clientResponse.getStatus());
						remoteToolsInError.put(extModuleList.get(i).getModuleName(), Utils.getErrorDescriptionByCode(clientResponse.getStatus()));
					}

					output = clientResponse.getEntity(String.class);

					if(output.substring(0, 1).compareTo("\"") == 0 && output.substring(output.length() - 1, output.length()).compareTo("\"") == 0) {
						output = output.substring(1, output.length() - 1);
					}

					output = output.replace("\\", "");

					response = JSONFactoryUtil.createJSONObject(output);
					_log.info("Response: " + response.toString());

					t = response.getInt("type");
					m = response.getInt("message");

					switch(t) {
						case MESSAGE_TYPE_SUCCESS:
							type = "SUCCESS"; break;
						case MESSAGE_TYPE_ERROR:
							type = "ERROR"; break;
						case MESSAGE_TYPE_WARNING:
							type = "WARNING"; break;
						default:
							type = "UNKNOWN";
					}

					switch(m) {
						case MESSAGE_REGISTRATION_SUCCEED:
							message = "Registration succeed"; break;
						case MESSAGE_REGISTRATION_FAILED:
							message = "Registration failed"; break;
						case MESSAGE_USER_EXISTS:
							message = "User already exists"; break;
						case MESSAGE_EMAIL_NOT_SENT:
							message = "Registration email not sent"; break;
						case MESSAGE_JSON_ERROR:
							message = "External module error in parsing JSON"; break;
						case MESSAGE_INVALID_SECRET:
							message = "Invalid secret"; break;
						case MESSAGE_DECRYPTION_ERROR:
							message = "External module error in decrypting request"; break;
						default:
							message = "Unknown";
					}

					_log.info(type + ": " + message);
				} catch(Exception e) {
    				String errorSending = "Error sending data to "+url+". "+e.getMessage();
    				_log.error(errorSending);
    				remoteToolsInError.put(extModuleList.get(i).getModuleName(), errorSending);
				}
			}
		} catch(Exception e) {
			_log.error(e);
		}

		return "OK";
	}

}
