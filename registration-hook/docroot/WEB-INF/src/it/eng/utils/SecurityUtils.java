package it.eng.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;

import javax.crypto.Cipher;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.model.PortalConf;
import it.eng.service.PortalConfLocalServiceUtil;

public class SecurityUtils 
{
	private static Log _log = LogFactoryUtil.getLog(SecurityUtils.class);
	
	private static String ecryptionAlgorithm = "RSA";
	
	public static Key getPrivateKey()
	{
		PortalConf portalConf = null;
		
		try 
		{
			portalConf = PortalConfLocalServiceUtil.getSecurityConfiguration();
		} 
		catch (SystemException e1) 
		{
			_log.error("Error retrieving portal configurations");
			e1.printStackTrace();
			return null;
		}
		
		String alias = portalConf.getTomcatKsAlias();
		String password = portalConf.getTomcatKsPassword();
		String location = portalConf.getTomcatKsLocation();
		
		FileInputStream is = null;
		Key private_key = null;
		
		try
		{
	        File file = new File(location);
	        is = new FileInputStream(file);
	        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			
			private_key = keystore.getKey(alias, password.toCharArray());
			
			if(private_key == null)
				_log.error("No key for alias '" + alias + "' in the keystore");
		}
		catch (FileNotFoundException e) 
		{
	        e.printStackTrace();
	    } 
		catch (KeyStoreException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnrecoverableKeyException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (NoSuchAlgorithmException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally 
		{
	        if(is != null)
	            try {
	                is.close();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	    }
		
		return private_key;
	}
	
	public static PublicKey getPublicKeyByAlias(String alias)
	{
		FileInputStream is = null;
		
		PortalConf portalConf = null;
		
		try 
		{
			portalConf = PortalConfLocalServiceUtil.getSecurityConfiguration();
		} 
		catch (SystemException e1) 
		{
			_log.error("Error retrieving portal configurations");
			e1.printStackTrace();
			return null;
		}
		
		try {
			String location = portalConf.getJvmKsLocation();
	        File file = new File(location);
	        is = new FileInputStream(file);
	        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
	        String password = portalConf.getJvmKsPassword();
	        keystore.load(is, password.toCharArray());
	        
	        Certificate certificate = keystore.getCertificate(alias);
	        
	        PublicKey pk = null;
	        
	        if(certificate != null)
	        	pk = certificate.getPublicKey();
	        else
	        	_log.error("No certificate for alias '" + alias + "' in the keystore");
	        
	        return pk;

	    } 
		catch (java.security.cert.CertificateException e) 
		{
	        e.printStackTrace();
	    } 
		catch (NoSuchAlgorithmException e) 
		{
	        e.printStackTrace();
	    } 
		catch (FileNotFoundException e) 
		{
	        e.printStackTrace();
	    } 
		catch (KeyStoreException e) 
		{
	        e.printStackTrace();
	    } 
		catch (IOException e) 
		{
	        e.printStackTrace();
	    }
		finally 
		{
	        if(is != null)
	            try {
	                is.close();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	    }
		
		return null;
	}
	
	public static byte[] encrypt(String text, PublicKey key) 
	{
	    byte[] cipherText = null;
	    try 
	    {
	      // get an RSA cipher object and print the provider
	      final Cipher cipher = Cipher.getInstance(ecryptionAlgorithm);
	      // encrypt the plain text using the public key
	      cipher.init(Cipher.ENCRYPT_MODE, key);
	      cipherText = cipher.doFinal(text.getBytes());
	    } 
	    catch (Exception e) 
	    {
	      e.printStackTrace();
	    }
	    
	    return cipherText;
	}
	
	public static String decrypt(byte[] text, Key key) 
	{
	    byte[] dectyptedText = null;
	    try 
	    {
	      // get an RSA cipher object and print the provider
	      final Cipher cipher = Cipher.getInstance(ecryptionAlgorithm);

	      // decrypt the text using the private key
	      cipher.init(Cipher.DECRYPT_MODE, key);
	      dectyptedText = cipher.doFinal(text);

	    } 
	    catch (Exception ex) 
	    {
	      ex.printStackTrace();
	    }

	    return new String(dectyptedText);
	  }
}
