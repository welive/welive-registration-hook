package it.eng.registration.hook;

import it.eng.utils.Constants;
import it.eng.utils.Propagate;
import it.eng.utils.PropertyGetter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.UriBuilder;

import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.ContactBirthdayException;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.struts.BaseStrutsPortletAction;
import com.liferay.portal.kernel.struts.StrutsPortletAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.CountryConstants;
import com.liferay.portal.model.ListTypeConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.model.RegionConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class CustomCreateAccountAction extends BaseStrutsPortletAction {
	/**
	 * This is the custom process action
	 *
	 * In this process action we are reading custom field which is entered from
	 * create account form
	 *
	 * After reading the custom field we are calling the original process action
	 */
	
	private final static Map<String, String> PILOT_LOCALE_MAP;
	static {
	    Map<String, String> map = new HashMap<String, String>(Constants.PILOT_CITY_LIST.length);
	    map.put(Constants.PILOT_CITY_TRENTO, "it_IT");
	    map.put(Constants.PILOT_CITY_BILBAO, "es_ES");
	    map.put(Constants.PILOT_CITY_NOVISAD, "sr_RS");
	    map.put(Constants.PILOT_CITY_HELSINKI, "fi_FI");
	    PILOT_LOCALE_MAP = Collections.unmodifiableMap(map);
	}
	
	private final static Map<String, String[]> LOCALE_LANGUAGE_MAP;
	static {
	    Map<String, String[]> map = new HashMap<String, String[]>(Constants.PILOT_CITY_LIST.length);
	    map.put("it_IT", new String[] {Constants.LANGUAGE_ITALIAN});
	    map.put("es_ES", new String[] {Constants.LANGUAGE_SPANISH});
	    map.put("sr_RS", new String[] {Constants.LANGUAGE_SERBIAN, Constants.LANGUAGE_SERBIAN_LATIN});
	    map.put("fi_FI", new String[] {Constants.LANGUAGE_FINNISH});
	    LOCALE_LANGUAGE_MAP = Collections.unmodifiableMap(map);
	}
	
	private String aacUrlCreateUser;
	private String aacUsername;
	private String aacPassword;

	private static Log _log = LogFactoryUtil
			.getLog(CustomCreateAccountAction.class);
	
	public CustomCreateAccountAction() {
		try {
			aacUrlCreateUser = PrefsPropsUtil.getString("welive.aac.url.user.create");
			aacUsername = PrefsPropsUtil.getString("welive.username", PropertyGetter.getProperty("welive.username"));
			aacPassword = PrefsPropsUtil.getString("welive.password", PropertyGetter.getProperty("welive.password"));
		} catch (SystemException e) {
			aacUrlCreateUser = PropertyGetter.getProperty("welive.aac.url.user.create");
			aacUsername = PropertyGetter.getProperty("welive.username");
			aacPassword = PropertyGetter.getProperty("welive.password");
		}
	}
	

	private String getOrganizationName(String pilotName, String roleName) {
		return String.format("%s %s", pilotName, roleName);
	}
	

	private long getOrganizationId(long companyId, String pilotName, String roleName) {
		try {
			String orgName = getOrganizationName(pilotName, roleName);

			DynamicQuery dq = DynamicQueryFactoryUtil.forClass(Organization.class)
					.add(PropertyFactoryUtil.forName("companyId").eq(companyId))
					.add(PropertyFactoryUtil.forName("name").eq(orgName));

			List<Organization> orgList = OrganizationLocalServiceUtil.dynamicQuery(dq);

			if (!orgList.isEmpty()) {
				// Organization exists.
				return OrganizationLocalServiceUtil.getOrganizationId(companyId, orgName);
			}
			
			// Organization does not exist. Create it.
			Company company = null;
			company = CompanyLocalServiceUtil.getCompany(companyId);

			ServiceContext ctx = new ServiceContext();
			ctx.setCompanyId(companyId);
			Organization organization = OrganizationLocalServiceUtil.addOrganization(
					company.getDefaultUser().getUserId(),
					OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID,
					orgName,
					OrganizationConstants.TYPE_REGULAR_ORGANIZATION,
					RegionConstants.DEFAULT_REGION_ID,
					CountryConstants.DEFAULT_COUNTRY_ID,
					ListTypeConstants.ORGANIZATION_STATUS_DEFAULT,
					"",
					false,
					ctx);
			return organization.getOrganizationId();
		} catch (Exception e) {
			_log.error(e);
			return 0;
		}
	}
	

	@Override
	public void processAction(StrutsPortletAction originalStrutsPortletAction,
			PortletConfig portletConfig, ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {

		String firstName = actionRequest.getParameter("firstName");
		String lastName = actionRequest.getParameter("lastName");
		String email = actionRequest.getParameter("emailAddress");
		// Other fields
//		String screenName = actionRequest.getParameter("screenName");
//		String middleName = actionRequest.getParameter("middleName");
		String birthday = actionRequest.getParameter("birthday");
		String birthdayDay = actionRequest.getParameter("birthdayDay");
		String birthdayMonth = actionRequest.getParameter("birthdayMonth");
		String birthdayYear = actionRequest.getParameter("birthdayYear");
		String gender = actionRequest.getParameter("male");
		// Custom fields
		String pilot = actionRequest.getParameter("pilot");
		String roleName = actionRequest.getParameter("role");
		String employment = actionRequest.getParameter("employment");
		String sIsDeveloper = actionRequest.getParameter("isDeveloper");
		String address = actionRequest.getParameter("address");
		String city = actionRequest.getParameter("city");
		String zipCode = actionRequest.getParameter("zipCode");
		String country = actionRequest.getParameter("country");
		String tagsComma = actionRequest.getParameter("tags");
		String skillsComma = actionRequest.getParameter("skills");
		String sisCompany = actionRequest.getParameter("isCompany");
		String companyName = actionRequest.getParameter("companyName");
		String companyDesc = actionRequest.getParameter("companyDesc");
		Organization organization = null;
		_log.info("birthday: "+birthday);
		_log.info("birthdayDay; "+birthdayDay);
		_log.info("birthdayMonth; "+birthdayMonth);
		_log.info("birthdayYear; "+birthdayYear);
		//int auxmonth = new Integer(birthdayMonth).intValue()-1;
		//birthday = birthdayDay+"/"+auxmonth+"/"+birthdayYear;
		
		//_log.info("birthday: "+birthday);
//		Enumeration<String> enumNames = actionRequest.getParameterNames();
//		while(enumNames.hasMoreElements()) {
//			String name = enumNames.nextElement();
//			_log.info("PARAMETER: " + name);
//		}
		
		// As a new organization can be created from the pilot name, is important to check the validity of the pilot name
		boolean pilotOk = false;
		for(String pilotKey : Constants.PILOT_CITY_LIST) {
			if(pilotKey.equals(pilot)) {
				pilotOk = true;
				break;
			}
		}
		if(!pilotOk) {
			SessionErrors.add(actionRequest, "error_pilot_name");
			return;
		}
		
		String sLocale = null;
		if(pilot != null) {
			sLocale = PILOT_LOCALE_MAP.get(pilot);
		}
		
		List<String> languageList = new ArrayList<String>();
		for(String languageKey : Constants.LANGUAGE_LIST) {
			String languageValue = actionRequest.getParameter("language_" + languageKey + "Checkbox");
			if(languageValue != null) {
				languageList.add(languageKey);
			}
		}
		if(sLocale != null) {
			String[] localeLanguages = LOCALE_LANGUAGE_MAP.get(sLocale);
			if(localeLanguages != null) {
				for(String localeLanguage : localeLanguages) {
					if(!languageList.contains(localeLanguage)) {
						languageList.add(localeLanguage);
					}
				}
			}
		}
		String[] languages = languageList.toArray(new String[languageList.size()]);

		if(lastName == null) {
			lastName = "";
		}
		if(birthday == null) {
			//SessionErrors.add(actionRequest, ContactBirthdayException.class);
			//return;
			birthday = "";
		}
	
		if(gender == null) {
			gender = "";
		}
		if(address == null) {
			address = "";
		}
		if(city == null) {
			city = "";
		}
		if(zipCode == null) {
			zipCode = "";
		}
		if(country == null) {
			country = "";
		}
		if(roleName == null) {
			roleName = Constants.ROLE_CITIZEN;
		}
		if(employment == null) {
			employment = "";
		}
		if(tagsComma == null) {
			tagsComma = "";
		}
		// TODO: "skills" not yet available
		if(skillsComma == null) {
			skillsComma = "";
		}
		if(companyName == null) {
			companyName = "";
		}
		if(companyDesc == null) {
			companyDesc = "";
		}
		
		boolean isDeveloper = (sIsDeveloper != null && (Constants.YES.equalsIgnoreCase(sIsDeveloper) | Constants.TRUE.equalsIgnoreCase(sIsDeveloper)));
		
		boolean isCompany = (sisCompany != null && (Constants.YES.equalsIgnoreCase(sisCompany) | Constants.TRUE.equalsIgnoreCase(sisCompany)));
		
		ArrayList<String> tagsList = new ArrayList<String>();
		for(String tag : tagsComma.split(",")) {
			tag = tag.trim();
			tag = tag.replaceAll("\\s+", " ");
			if(tag.length() > 0 && !" ".equals(tag)) {
				tagsList.add(tag);
			}
		}
		String[] tagsArray = tagsList.toArray(new String[tagsList.size()]);

		ArrayList<String> skillsList = new ArrayList<String>();
		for(String skill : skillsComma.split(",")) {
			skill = skill.trim();
			skill = skill.replaceAll("\\s+", " ");
			if(skill.length() > 0 && !" ".equals(skill)) {
				skillsList.add(skill);
			}
		}
		String[] skillsArray = skillsList.toArray(new String[skillsList.size()]);
		
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		User user = null;
		try {
			user = UserLocalServiceUtil.getUserByEmailAddress(
				themeDisplay.getCompanyId(), email);
		}
		catch(com.liferay.portal.NoSuchUserException ex){
			//If the user doesn't exist an exception is raised.
		}
		if (user != null ){
			SessionErrors.add(actionRequest, DuplicateUserEmailAddressException.class);
			return;
		}
		// AAC AddUser call
		Integer CCUserID = null;
		try {
			CCUserID = addAacUser(email, firstName, lastName);
			if(CCUserID <= 0) {
				SessionErrors.add(actionRequest, "error_aac_create_user");
			}
		} catch(Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, e.getClass(), e);
		}

		if (!SessionErrors.isEmpty(actionRequest)) {
			// Error
			// TODO: Roll back?
			return;
		}
		try{
			// Create Liferay user
			_log.info("Create Liferay user ");
			originalStrutsPortletAction.processAction(originalStrutsPortletAction,
					portletConfig, actionRequest, actionResponse);
			_log.info("Created !!");
		
		} catch (Exception e) {
			_log.error(e.getMessage());
			SessionErrors.add(actionRequest, e.getClass(), e);
		}
		
		if (!SessionErrors.isEmpty(actionRequest)) {
			// Error
			// TODO: Roll back?
			return;
		}

		themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		user = UserLocalServiceUtil.getUserByEmailAddress(
				themeDisplay.getCompanyId(), email);
		long userId = user.getUserId();

		try {
			if(gender.isEmpty()) {
				// Default gender is male, not female
				Contact contact = ContactLocalServiceUtil.getContact(user.getContactId());
				contact.setMale(true);
				ContactLocalServiceUtil.updateContact(contact);
			}
			
			String[] pilotArray = { pilot };
			
			ArrayList<String> employmentList = new ArrayList<String>();
			if(!employment.isEmpty()) {
				employmentList.add(employment);
			}
			String[] employmentArray = employmentList.toArray(new String[employmentList.size()]);
			
			String webId = new String("liferay.com");
			Company company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
			long companyId = company.getCompanyId();
			_log.info("companyId: " + companyId);
			// Associate user-role
			Role role = RoleLocalServiceUtil.getRole(companyId, roleName);
			UserLocalServiceUtil.addRoleUser(role.getRoleId(), userId);
			long orgId = getOrganizationId(companyId, pilot, roleName);
			UserLocalServiceUtil.addOrganizationUser(orgId, userId);
			_log.info("Associate user-role " );
			
			if(isDeveloper) {
				Role roleDeveloper = RoleLocalServiceUtil.getRole(companyId, Constants.ROLE_DEVELOPER);
				UserLocalServiceUtil.addRoleUser(roleDeveloper.getRoleId(), userId);
			}
			
			if(isCompany) {
				//com.liferay.portal.DuplicateOrganizationException: There is another organization named TCN
				try{
					organization = OrganizationLocalServiceUtil.addOrganization(
							company.getDefaultUser().getUserId(),
							OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID,
							companyName,
							OrganizationConstants.TYPE_REGULAR_ORGANIZATION,
							RegionConstants.DEFAULT_REGION_ID,
							CountryConstants.DEFAULT_COUNTRY_ID,
							ListTypeConstants.ORGANIZATION_STATUS_DEFAULT,
							companyDesc,
							false,
							new ServiceContext());
	
					long newOrgId= organization.getOrganizationId();
					_log.info("Org created:" +newOrgId);
					UserLocalServiceUtil.addOrganizationUser(newOrgId, userId);
					_log.info("User added to org");	
					Role roleCompany = RoleLocalServiceUtil.getRole(companyId, Constants.ROLE_COMPANY_LEADER);
					UserLocalServiceUtil.addRoleUser(roleCompany.getRoleId(), userId);
					// Add org extra fields
					String className = Organization.class.getName();
					long classNameId = ClassNameLocalServiceUtil.getClassNameId(className);
					ExpandoTable expandoTable = ExpandoTableLocalServiceUtil.getDefaultTable(company.getCompanyId(), classNameId);
					ExpandoColumn expandoColumn = ExpandoColumnLocalServiceUtil.getColumn(expandoTable.getTableId(), "isCompany");
					ExpandoValueLocalServiceUtil.addValue(classNameId ,expandoTable.getTableId(), expandoColumn.getColumnId(), newOrgId, sisCompany);
					_log.info("Fiel isCompany added");	
				} catch (Exception e) {
					_log.error(e);
					SessionErrors.add(actionRequest, e.getClass(), e);
					return;
				}				
			}
			
			if(sLocale != null) {
				user.setLanguageId(sLocale);
			}
			
			UserLocalServiceUtil.updateUser(user);

			// Add user extra fields
			String className = User.class.getName();
			long classNameId = ClassNameLocalServiceUtil.getClassNameId(className);
			ExpandoTable expandoTable = ExpandoTableLocalServiceUtil.getDefaultTable(company.getCompanyId(), classNameId);
			
			Map<String, Serializable> expandoBridgeAttributes = new HashMap<String, Serializable>();
			expandoBridgeAttributes.put("CCUserID", CCUserID);
			expandoBridgeAttributes.put("pilot", pilotArray);
			expandoBridgeAttributes.put("employement", employmentArray);
			expandoBridgeAttributes.put("address", address);
			expandoBridgeAttributes.put("city", city);
			expandoBridgeAttributes.put("zipCode", zipCode);
			expandoBridgeAttributes.put("country", country);
			expandoBridgeAttributes.put("languages", languages);
			/*if (!companyName.equals("")){
				expandoBridgeAttributes.put("companyName", companyName);
			}
			if (!companyDesc.equals("")){

				expandoBridgeAttributes.put("companyDesc", companyDesc);
			}
			expandoBridgeAttributes.put("isCompany", sisCompany);
			*/
			for (Map.Entry<String, Serializable> entry : expandoBridgeAttributes.entrySet()) {
				ExpandoColumn expandoColumn =  ExpandoColumnLocalServiceUtil.getColumn(themeDisplay.getCompanyId(), classNameId, expandoTable.getName(), entry.getKey());
				ExpandoValueLocalServiceUtil.addValue(company.getCompanyId(), className, expandoTable.getName(), expandoColumn.getName(), userId, entry.getValue());
			}
			
			// Redundant, but it seems to give problems on dev.welive.eu
			HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
			HttpSession session = request.getSession();
			String openId = ParamUtil.getString(actionRequest, "openId");
			boolean openIdPending = false;
			Boolean openIdLoginPending = (Boolean)session.getAttribute("OPEN_ID_LOGIN_PENDING");
			if ((openIdLoginPending != null) && openIdLoginPending.booleanValue() && Validator.isNotNull(openId)) {
				openIdPending = true;
			}
			if (openIdPending) {
				session.setAttribute("OPEN_ID_LOGIN", Long.valueOf(user.getUserId()));
				session.removeAttribute("OPEN_ID_LOGIN_PENDING");
				_log.info("OpenId pending: " + user.getUserId());
			} else {
				if (user.getStatus() == WorkflowConstants.STATUS_APPROVED) {
					SessionMessages.add(request, "userAdded", user.getEmailAddress());
					SessionMessages.add(request, "userAddedPassword", user.getPasswordUnencrypted());
					_log.info("User added: " + user.getEmailAddress());
				} else {
					SessionMessages.add(request, "userPending", user.getEmailAddress());
					_log.info("User pending: " + user.getEmailAddress());
				}
			}
			
		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, e.getClass(), e);
		}

		if (!SessionErrors.isEmpty(actionRequest)) {
			// Error
			// TODO: Roll back?
			return;
		}

		// TODO: Propagate "employement" when ready
		// Propagate the user creation event to other BB's
		try {
			Boolean bGender = null;
			Integer iBirthdayDay = null;
			Integer iBirthdayMonth = null;
			Integer iBirthdayYear = null;
			if(!gender.isEmpty()) {
				bGender = user.isMale();
			}
			if(!birthday.isEmpty()) {
				Date birthdayDate = user.getBirthday();
				Calendar birthdayCalendar = Calendar.getInstance();
				birthdayCalendar.setTime(birthdayDate);
				iBirthdayDay = birthdayCalendar.get(Calendar.DAY_OF_MONTH);
				iBirthdayMonth = birthdayCalendar.get(Calendar.MONTH) + 1;
				iBirthdayYear = birthdayCalendar.get(Calendar.YEAR);
			}
			String code = Propagate.getInstance().userPropagate(user.getUserId(), 
					CCUserID,
					pilot,
					user.getFirstName(), 
//					user.getLastName(),
					lastName,
					bGender,
					iBirthdayDay,
					iBirthdayMonth,
					iBirthdayYear,
					user.getEmailAddress(),
					isDeveloper,
					city,
					address,
					country,
					zipCode,
					languages,
					roleName,
					tagsArray,
					skillsArray,
					employment,
					isCompany
					);
			
			if (!"OK".equals(code)) {
				_log.error("The propagation of the user creation could not be performed successfully.");
				SessionErrors.add(actionRequest, "error_propagation");
			}
			//Propagate Organization if isCompany == true			
			if (isCompany){
				code = Propagate.getInstance().organizationPropagate( organization.getOrganizationId(), organization.getName(), "", CCUserID);
				if (!"OK".equals(code)) {
					_log.error("The propagation of the organization creation could not be performed successfully.");
					SessionErrors.add(actionRequest, "error_propagation");
				}
				
			}
				
		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, e.getClass(), e);
		}
	}
	

	@Override
	public String render(StrutsPortletAction originalStrutsPortletAction,
			PortletConfig portletConfig, RenderRequest renderRequest,
			RenderResponse renderResponse) throws Exception {
		String termsAgree = renderRequest.getParameter("terms_agree");
		if(termsAgree != null) {
			return originalStrutsPortletAction.render(originalStrutsPortletAction,
					portletConfig, renderRequest, renderResponse);
		} else {
			return "/portal/terms_of_use_create.jsp";
		}

	}
	
	
	/**
	 * AAC AddUser call
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @return CCUserID
	 */
	private int addAacUser(String email, String firstName, String lastName) {
		int CCUserID = -1;
		
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter(aacUsername, aacPassword));
		WebResource webResource = client.resource(UriBuilder.fromUri(aacUrlCreateUser).build());

		JSONObject requestJson = JSONFactoryUtil.createJSONObject();
		requestJson.put("name", firstName);
		requestJson.put("surname", lastName);
		requestJson.put("email", email);

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, requestJson.toString());

		int responseStatus = response.getStatus();
		if (responseStatus != 200) {
			_log.error("AAC error creating user: HTTP Error Code: " + responseStatus);
		} else {
			String responseEntity = response.getEntity(String.class);
			try{
				CCUserID = Integer.parseInt(responseEntity);
				if(CCUserID > 0) {
					_log.info("CCUserID: " + CCUserID);
				} else {
					_log.error("AAC error creating user: CCUserID: " + CCUserID);
				}
			}
			catch(Exception e){
				_log.error("Exception in AAC creating user, responseEntuty:"+responseEntity+"Exception:" + e.getMessage());
			}
		}
		
		return CCUserID;
	}

}
