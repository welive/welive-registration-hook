<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<%@page import="javax.portlet.PortletSession"%>

<%
String pilot = (String)portletSession.getAttribute("LIFERAY_SHARED_pilot", PortletSession.APPLICATION_SCOPE);
try {
	if(themeDisplay.isSignedIn() && user != null) {
		String[] pilotArray = (String [])user.getExpandoBridge().getAttribute("pilot");
		if(pilotArray != null && pilotArray.length > 0) {
			pilot = pilotArray[0];
			portletSession.setAttribute("LIFERAY_SHARED_pilot", pilot);
		}
	}
} catch(Exception e) {}
%>

<%if(pilot != null) {%>
<script>
if (window.sessionStorage) {
	sessionStorage.setItem("pilot", "<%=pilot%>");
}
</script>
<%}%>