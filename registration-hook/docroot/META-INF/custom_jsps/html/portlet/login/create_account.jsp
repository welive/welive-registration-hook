<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<%@taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@page import="java.util.HashMap" %>
<%@page import="java.util.TreeMap" %>
<%@page import="java.util.GregorianCalendar" %>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@page import="com.liferay.portal.ContactBirthdayException" %>
<%@ include file="/html/portlet/login/init.jsp" %>

<%
String redirect = ParamUtil.getString(request, "redirect");

String openId = ParamUtil.getString(request, "openId");
Boolean male = null;
String sMale = request.getParameter("male");
if(sMale != null && !sMale.isEmpty()) {
	male = Boolean.parseBoolean(sMale);
}
String sBirthday = request.getParameter("birthday");
String sBirthdayDay = request.getParameter("birthdayDay");
String sBirthdayMonth = request.getParameter("birthdayMonth");
String sBirthdayYear = request.getParameter("birthdayYear");

Map<String, String> langMap = new HashMap<String, String>();
langMap.put(LanguageUtil.get(locale, "english"), "language_English");
langMap.put(LanguageUtil.get(locale, "finnish"), "language_Finnish");
langMap.put(LanguageUtil.get(locale, "italian"), "language_Italian");
langMap.put(LanguageUtil.get(locale, "serbian"), "language_Serbian");
langMap.put(LanguageUtil.get(locale, "serbian_latin"), "language_SerbianLatin");
langMap.put(LanguageUtil.get(locale, "spanish"), "language_Spanish");
Map<String, String> langTreeMap = new TreeMap<String, String>(langMap);

Map<String, String> countryMap = new HashMap<String, String>();
countryMap.put(LanguageUtil.get(locale, "finland"), "Finland");
countryMap.put(LanguageUtil.get(locale, "italy"), "Italy");
countryMap.put(LanguageUtil.get(locale, "serbia"), "Serbia");
countryMap.put(LanguageUtil.get(locale, "spain"), "Spain");
Map<String, String> countryTreeMap = new TreeMap<String, String>(countryMap);

Calendar birthdayCalendar = CalendarFactoryUtil.getCalendar();

birthdayCalendar.set(Calendar.MONTH, Calendar.JANUARY);
birthdayCalendar.set(Calendar.DATE, 1);
birthdayCalendar.set(Calendar.YEAR, 1900);
///////////////////////
Date now = new Date();

Calendar calendar = new GregorianCalendar();
calendar.setTime(now);
int year = calendar.get(Calendar.YEAR);
int maxyear = year - 16;
int maxmonth = calendar.get(Calendar.MONTH)+1;
String smaxmonth = ""+maxmonth;
if (smaxmonth.length()<2) smaxmonth ="0"+smaxmonth;
int maxday = calendar.get(Calendar.DAY_OF_MONTH);
String smaxday = ""+maxday;
if (smaxday.length()<2) smaxday ="0"+smaxday;

boolean showtag = true;
String userAgent = request.getHeader("User-Agent");

if (userAgent.contains("Chrome") || userAgent.contains("Edge")|| userAgent.contains("Opera")){
	showtag = false;
}
%>

<portlet:actionURL secure="<%= PropsValues.COMPANY_SECURITY_AUTH_REQUIRES_HTTPS || request.isSecure() %>" var="createAccountURL">
	<portlet:param name="struts_action" value="/login/create_account" />
</portlet:actionURL>

<aui:form action="<%= createAccountURL %>" method="post" name="fm">
	<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.ADD %>" />
	<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
	<aui:input name="openId" type="hidden" value="<%= openId %>" />
	<aui:input name="terms_agree" type="hidden" value="<%= true %>" />
	
	<liferay-ui:error exception="<%= ContactBirthdayException.class %>" message="please-enter-a-valid-date" />

	<liferay-ui:error exception="<%= AddressCityException.class %>" message="please-enter-a-valid-city" />
	<liferay-ui:error exception="<%= AddressStreetException.class %>" message="please-enter-a-valid-street" />
	<liferay-ui:error exception="<%= AddressZipException.class %>" message="please-enter-a-valid-postal-code" />
	<liferay-ui:error exception="<%= CaptchaMaxChallengesException.class %>" message="maximum-number-of-captcha-attempts-exceeded" />
	<liferay-ui:error exception="<%= CaptchaTextException.class %>" message="text-verification-failed" />
	<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-create-user-account-because-the-maximum-number-of-users-has-been-reached" />
	<liferay-ui:error exception="<%= ContactFirstNameException.class %>" message="please-enter-a-valid-first-name" />
	<liferay-ui:error exception="<%= ContactFullNameException.class %>" message="please-enter-a-valid-first-middle-and-last-name" />
	<liferay-ui:error exception="<%= ContactLastNameException.class %>" message="please-enter-a-valid-last-name" />
	<liferay-ui:error exception="<%= DuplicateOpenIdException.class %>" message="a-user-with-that-open-id-already-exists" />
	<liferay-ui:error exception="<%= DuplicateUserEmailAddressException.class %>" message="the-email-address-you-requested-is-already-taken" />
	<liferay-ui:error exception="<%= DuplicateUserIdException.class %>" message="the-user-id-you-requested-is-already-taken" />
	<liferay-ui:error exception="<%= DuplicateUserScreenNameException.class %>" message="the-screen-name-you-requested-is-already-taken" />
	<liferay-ui:error exception="<%= EmailAddressException.class %>" message="please-enter-a-valid-email-address" />

	<liferay-ui:error exception="<%= GroupFriendlyURLException.class %>">

		<%
		GroupFriendlyURLException gfurle = (GroupFriendlyURLException)errorException;
		%>

		<c:if test="<%= gfurle.getType() == GroupFriendlyURLException.DUPLICATE %>">
			<liferay-ui:message key="the-screen-name-you-requested-is-associated-with-an-existing-friendly-url" />
		</c:if>
	</liferay-ui:error>

	<liferay-ui:error exception="<%= NoSuchCountryException.class %>" message="please-select-a-country" />
	<liferay-ui:error exception="<%= NoSuchListTypeException.class %>" message="please-select-a-type" />
	<liferay-ui:error exception="<%= NoSuchRegionException.class %>" message="please-select-a-region" />
	<liferay-ui:error exception="<%= PhoneNumberException.class %>" message="please-enter-a-valid-phone-number" />
	<liferay-ui:error exception="<%= RequiredFieldException.class %>" message="please-fill-out-all-required-fields" />
	<liferay-ui:error exception="<%= ReservedUserEmailAddressException.class %>" message="the-email-address-you-requested-is-reserved" />
	<liferay-ui:error exception="<%= ReservedUserIdException.class %>" message="the-user-id-you-requested-is-reserved" />
	<liferay-ui:error exception="<%= ReservedUserScreenNameException.class %>" message="the-screen-name-you-requested-is-reserved" />
	<liferay-ui:error exception="<%= TermsOfUseException.class %>" message="you-must-agree-to-the-terms-of-use" />
	<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="please-enter-a-valid-email-address" />
	<liferay-ui:error exception="<%= UserIdException.class %>" message="please-enter-a-valid-user-id" />

	<liferay-ui:error exception="<%= UserPasswordException.class %>">

		<%
		UserPasswordException upe = (UserPasswordException)errorException;
		%>

		<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_CONTAINS_TRIVIAL_WORDS %>">
			<liferay-ui:message key="that-password-uses-common-words-please-enter-in-a-password-that-is-harder-to-guess-i-e-contains-a-mix-of-numbers-and-letters" />
		</c:if>

		<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_INVALID %>">
			<liferay-ui:message key="that-password-is-invalid-please-enter-in-a-different-password" />
		</c:if>

		<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_LENGTH %>">

			<%
			PasswordPolicy passwordPolicy = PasswordPolicyLocalServiceUtil.getDefaultPasswordPolicy(company.getCompanyId());
			%>

			<%= LanguageUtil.format(pageContext, "that-password-is-too-short-or-too-long-please-make-sure-your-password-is-between-x-and-512-characters", String.valueOf(passwordPolicy.getMinLength()), false) %>
		</c:if>

		<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_TOO_TRIVIAL %>">
			<liferay-ui:message key="that-password-is-too-trivial" />
		</c:if>

		<c:if test="<%= upe.getType() == UserPasswordException.PASSWORDS_DO_NOT_MATCH %>">
			<liferay-ui:message key="the-passwords-you-entered-do-not-match-each-other-please-re-enter-your-password" />
		</c:if>
	</liferay-ui:error>

	<liferay-ui:error exception="<%= UserScreenNameException.class %>" message="please-enter-a-valid-screen-name" />
	<liferay-ui:error exception="<%= WebsiteURLException.class %>" message="please-enter-a-valid-url" />

	<c:if test='<%= SessionMessages.contains(request, "openIdUserInformationMissing") %>'>
		<div class="alert alert-info" style="display:block !important">
			<liferay-ui:message key="you-have-successfully-authenticated-please-provide-the-following-required-information-to-access-the-portal" />
		</div>
	</c:if>

	<aui:model-context model="<%= Contact.class %>" />

	<aui:fieldset column="<%= true %>">
	
		<aui:col width="<%= 25 %>">

			<aui:select showEmptyOption="true" autoFocus="<%= true %>" label="pilot_city" name="pilot" helpMessage="pilot_city_tooltip" required="true" >
				<aui:option label="Bilbao" value="Bilbao" />
				<aui:option label="Novi Sad" value="Novisad" />
				<aui:option label="Region on Uusimaa-Helsinki" value="Uusimaa" />
				<aui:option label="Trento" value="Trento" />
			</aui:select>
<%-- 
			<%@ include file="/html/portlet/login/create_account_user_name.jspf" %>
--%>
	
			<aui:input model="<%= User.class %>" name="firstName" />
			
			<aui:input model="<%= User.class %>" name="lastName">
				<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.USERS_LAST_NAME_REQUIRED, PropsValues.USERS_LAST_NAME_REQUIRED) %>">
					<aui:validator name="required" />
				</c:if>
			</aui:input>
	
			<aui:input  model="<%= User.class %>" name="emailAddress">
				<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.USERS_EMAIL_ADDRESS_REQUIRED) %>">
					<aui:validator name="required" />
				</c:if>
			</aui:input>
		
		
	

		</aui:col>
		<aui:col width="<%= 25 %>">
	<div class="control-group input-String-wrapper">  
			<label class="control-label" placeholder="yyy-mm-dd" id="<portlet:namespace />birthday_cl" for="<portlet:namespace />birthday_sw"><liferay-ui:message key="birthday" /> <%if(showtag){ %>(yyyy-mm-dd)<%} %>  </label>	
		    <input type="date" name="<portlet:namespace />birthday_sw" id="<portlet:namespace />birthday_sw" step="1" max="<%=maxyear%>-<%=smaxmonth%>-<%=smaxday%>" value="<%=maxyear%>-<%=smaxmonth%>-<%=smaxday%>" />
			<div id="<portlet:namespace />birthday_msg" class="form-validator-stack help-inline" style="display: none;">
				<div id="<portlet:namespace />birthday_txt" role="alert" class="required" style="color:#b50303;"><liferay-ui:message key="error_age_min" /></div>
			</div>		    
		</div>
		<input type="hidden" id="<portlet:namespace />birthday" name="<portlet:namespace />birthday" value=""/>
		<aui:input name="birthdayMonth" type="hidden" value="<%= Calendar.JANUARY %>" />
		<aui:input name="birthdayDay" type="hidden" value="1" />
		<aui:input name="birthdayYear" type="hidden" value="1900" />
		
			<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.FIELD_ENABLE_COM_LIFERAY_PORTAL_MODEL_CONTACT_MALE) %>">
				<aui:select showEmptyOption="true" label="gender" name="male">
					<aui:option label="male" selected="<%= male != null && male %>" value="1" />
					<aui:option label="female" selected="<%= male != null && !male %>" value="0" />
				</aui:select>
			</c:if>
		
			<aui:select showEmptyOption="true" label="work_status" name="employment" >
				<aui:option value="Student" label="student"/>
				<aui:option value="Unemployed" label="unemployed"/>
				<aui:option value="Employedbythirdparty" label="employed_3rd"/>
				<aui:option value="Selfemployedentrepreneur" label="self_entrepreneur"/>
				<aui:option value="Retired" label="retired"/>
				<aui:option value="Other" label="other" />
			</aui:select>
	
			<c:if test="<%= !PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.USERS_SCREEN_NAME_ALWAYS_AUTOGENERATE) %>">
				<aui:input model="<%= User.class %>" name="screenName" />
			</c:if>
							<aui:select label="is_developer" name="isDeveloper"  >
				<aui:option label="no" value="no" selected="<%= true %>" />
				<aui:option label="yes" value="yes" />
			</aui:select>

<%--			
			<aui:field-wrapper name="roles" label="roles">
				<aui:input name="role_Citizen" type="checkbox" label="citizen" />
				<aui:input name="role_Academy" type="checkbox" label="academy" />
				<aui:input name="role_Business" type="checkbox" label="business" />
				<aui:input name="role_Entrepreneur" type="checkbox" label="entrepreneur" />
				
				<aui:input name="roles" type="hidden" >
					<aui:validator name="custom" errorMessage="error_validation_checkbox_list">
						function (val, fieldNode, ruleValue) {
							return validate_checkbox_list(val, fieldNode, ruleValue);
						}
					</aui:validator>
				</aui:input>
			</aui:field-wrapper>
--%>
		
			<aui:select label="role" name="role" helpMessage="roles_tooltip" >
				<aui:option value="Citizen" label="citizen" selected="<%= true %>"/>
				<aui:option value="Academy" label="academy" />
				<aui:option value="Business" label="business" />
				<aui:option value="Entrepreneur" label="entrepreneur" />
			</aui:select>
		</aui:col>
		<aui:col width="<%= 25 %>">
		
			<aui:input name="middleName" type="hidden" value="" />
			
			<aui:input label="address" name="address" maxlength="100" type="text" value="">
				<aui:validator name="rangeLength">
					[ 2, 100 ]
				</aui:validator>
			</aui:input>
		
			<aui:input label="city" name="city" maxlength="40" type="text" value="">
				<aui:validator name="rangeLength">
					[ 2, 40 ]
				</aui:validator>
			</aui:input>
			 
			<aui:input label="zipcode" name="zipCode" maxlength="5" type="text" value="">
				<aui:validator name="digits">
					true
				</aui:validator>
				<aui:validator name="rangeLength">
					[ 5, 5 ]
				</aui:validator>
			</aui:input>
		
			<%-- TODO: Sort list --%>
			<aui:select label="country" name="country">
				<aui:option value="" />
<%
for (Map.Entry<String, String> entry : countryTreeMap.entrySet()) {
%>
				<aui:option label="<%= entry.getKey() %>" value="<%= entry.getValue() %>" />
<%
}
%>
				<aui:option label="other" value="Other" />
			</aui:select>

			<aui:select label="company" name="isCompany" helpMessage="company_tooltip" onchange="showCompany(this.value)">
				<aui:option value="false" label="No" selected="<%= true %>"/>
				<aui:option value="true" label="Yes" />
			</aui:select>
			
			<div id="showCompany" style="display:none">
			<aui:input label="company_name" name="companyName" maxlength="100" type="text" value="">
				<aui:validator name="rangeLength">
					[ 2, 100 ]
				</aui:validator>
			</aui:input>
		
			
			<aui:input label="company_desc" name="companyDesc"  rows="6" maxlength="400" type="textarea" value="">
				<aui:validator name="rangeLength">
					[ 2, 400 ]
				</aui:validator>
			</aui:input>
			</div>
		
			
			<c:if test="<%= PropsValues.LOGIN_CREATE_ACCOUNT_ALLOW_CUSTOM_PASSWORD %>">
				<aui:input label="password" name="password1" size="30" type="password" value="" />

				<aui:input label="enter-again" name="password2" size="30" type="password" value="">
					<aui:validator name="equalTo">
						'#<portlet:namespace />password1'
					</aui:validator>
				</aui:input>
			</c:if>
		
		</aui:col>
			

		<aui:col width="<%= 25 %>">


	
			
			<aui:field-wrapper name="languages" label="languages" helpMessage="languages_tooltip" >
<%
for (Map.Entry<String, String> entry : langTreeMap.entrySet()) {
%>
				<aui:input name="<%= entry.getValue() %>" type="checkbox" label="<%= entry.getKey() %>" />
<%
}
%>
			</aui:field-wrapper>
			
			<aui:input label="tags_comma" name="tags" maxlength="200" type="text" placeholder="placeholder_tags" value="" helpMessage="tags_tooltip">
				<aui:validator name="rangeLength">
					[ 2, 200 ]
				</aui:validator>
			</aui:input>

<%--
			<aui:input label="skills_comma" name="skills" maxlength="200" type="text" placeholder="skill1,skill2" value="" helpMessage="skills_tooltip">
				<aui:validator name="rangeLength">
					[ 2, 200 ]
				</aui:validator>
			</aui:input>
--%>

			<c:if test="<%= PropsValues.CAPTCHA_CHECK_PORTAL_CREATE_ACCOUNT %>">
				<portlet:resourceURL var="captchaURL">
					<portlet:param name="struts_action" value="/login/captcha" />
				</portlet:resourceURL>

				<liferay-ui:captcha url="<%= captchaURL %>" />
			</c:if>

		</aui:col>
		
	</aui:fieldset>
	
	<aui:input name="age_disclaimer" required="true" type="checkbox" label="age_disclaimer" >
		<aui:validator name="custom" errorMessage=" ">
			function (val, fieldNode, ruleValue) {
				return $('#<portlet:namespace />age_disclaimerCheckbox').is(':checked');
			}
		</aui:validator>
	</aui:input>
<div id="materialize-body" class="materialize">
	<aui:button-row>		
		<aui:button type="cancel" cssClass="red lighten-1 white-text btn btn-flat" href="/" value="cancel" />
		<aui:button type="button" cssClass="green lighten-1 white-text btn btn-flat" name="submit" value="save"/>
	</aui:button-row>
</div>
<%--
	<aui:button-row >
		<aui:button type="submit" />
	</aui:button-row>//http://www.liferaysavvy.com/2014/01/aui-form-validator-in-liferay.html
--%>
</aui:form>

<liferay-util:include page="/html/portlet/login/navigation.jsp" />


<aui:script>
	var <portlet:namespace />validator;
	AUI().use('aui-form-validator', function(A) {
		<portlet:namespace />validator = new A.FormValidator({
			boundingBox : '#<portlet:namespace />fm',
			rules : {
			},
			showAllMessages : false
		});
	});
</aui:script>
<link rel="stylesheet" type="text/css" href="/html/css/fix.css">

<script type="text/javascript" src="/html/js/custom_validation.js"></script>

<div id="modal_novisad" class="modal" style="display:none">
	<div class="modal-content">
		<h4><i class="material-icons blue-text">info</i> <liferay-ui:message key="registration.condition.novi_sad.title" /></h4>
		<liferay-ui:message key="registration.condition.novi_sad" />
	</div>
	<div class="modal-footer">
		<a href="#!" class=" modal-action modal-close red lighten-1 white-text btn btn-flat"><liferay-ui:message key="i-disagree"/></a>
		<a href="#!" class=" modal-action modal-close blue lighten-1 white-text btn btn-flat" id="modal_novisad_ok"><liferay-ui:message key="i-agree"/></a>
	</div>
</div>

<script>


	(function($) {
		var  jLanguageCheckboxForcedArray = [];

		function enableCheckbox(jElement, enable) {
			jElement.prop('disabled', !enable);
			jElement.prop('readonly', !enable);
		}

		function checkLanguagePilot() {
			var pilot = $('select[name=<portlet:namespace />pilot]').val();
			var jCbArray = [];
			if(pilot === 'Trento') {
				jCbArray = [$('#<portlet:namespace />language_ItalianCheckbox')];
			} else if(pilot === 'Bilbao') {
				jCbArray = [$('#<portlet:namespace />language_SpanishCheckbox')];
			} else if(pilot === 'Novisad') {
				jCbArray = [$('#<portlet:namespace />language_SerbianCheckbox'), $('#<portlet:namespace />language_SerbianLatinCheckbox')];
			} else if(pilot === 'Uusimaa') {
				jCbArray = [$('#<portlet:namespace />language_FinnishCheckbox')];
			}
			enableCheckbox($('#<portlet:namespace />language_ItalianCheckbox'), true);
			enableCheckbox($('#<portlet:namespace />language_SpanishCheckbox'), true);
			enableCheckbox($('#<portlet:namespace />language_SerbianCheckbox'), true);
			enableCheckbox($('#<portlet:namespace />language_FinnishCheckbox'), true);
			enableCheckbox($('#<portlet:namespace />language_SerbianLatinCheckbox'), true);
			enableCheckbox($('#<portlet:namespace />language_EnglishCheckbox'), true);
			if(jLanguageCheckboxForcedArray.length > 0) {
				for (var i = 0; i < jLanguageCheckboxForcedArray.length; i++) {
					jLanguageCheckboxForcedArray[i].prop('checked', false);
				}
			}
			jLanguageCheckboxForcedArray = [];
			for (var i = 0; i < jCbArray.length; i++) {
				var jCb = jCbArray[i];
				if(!jCb.is(':checked')) {
					jLanguageCheckboxForcedArray.push(jCb);
				}
				jCb.prop('checked', true);
				enableCheckbox(jCb, false);
			}
		}

		function isUnderAge(ageLimit) {
			var jElement = $('#<portlet:namespace />birthday_sw');
			var val = jElement.val();//2007-07-21  //01/01/2017
			var valid = false;
			var birthdate = null;
			var arrdate= null;
			document.getElementById("<portlet:namespace />birthday_txt").innerHTML='<liferay-ui:message key="error_age_min" />';

			if(!val) {
				return true;
			}
			try {
				var regexDMY = /^(\d{4})[.-](\d{2})[.-](\d{2})$/;
				if ((val.match(regexDMY)) && (val!='')) {
					arrdate = regexDMY.exec(val);
					console.log("isUnderAge:"+arrdate[1]+" "+arrdate[2] +" "+arrdate[3]);
					birthdate = new Date(arrdate[1],arrdate[2]-1 ,arrdate[3],	0,0,0	);
				}
				
			    else {
					birthdate = new Date(val);
					document.getElementById("<portlet:namespace />birthday_txt").innerHTML='<liferay-ui:message key="required"/>'+" "+'<liferay-ui:message key="format"/>'+ '(yyyy-mm-dd)' ;
					return true;
				}
				valid = !isNaN(birthdate);//<liferay-ui:message key="error_age_min" />
			} catch(e) {
				console.log("isUnderAge:"+e.message);
				document.getElementById("<portlet:namespace />birthday_txt").innerHTML='<liferay-ui:message key="required"/>'+" "+'<liferay-ui:message key="format"/>'+ '(yyyy-mm-dd)' ;
				return true;
			}
			if (valid) {
				var today = new Date();
				var age = today.getFullYear() - birthdate.getFullYear();
				var m = today.getMonth() - birthdate.getMonth();
				console.log("isUnderAge m:"+today.getMonth()+" "+birthdate.getMonth() );
				if (m < 0 || (m === 0 && (today.getDate() < birthdate.getDate()))) {
					age--;
				}
				console.log("isUnderAge:"+ age +" "+ageLimit);
				console.log("isUnderAge:"+ (age < ageLimit  ) );
				
				return  (age < ageLimit  ) ;
			}
			return true;
		}


		function setDefaultBirthday() {
			var birthdaysw = $('#<portlet:namespace />birthday_sw').val();
			if(!birthdaysw) {
				$('#<portlet:namespace />birthdayDay').val("1");
				$('#<portlet:namespace />birthdayMonth').val("<%= Calendar.JANUARY %>");
				$('#<portlet:namespace />birthdayYear').val("1900");
			}
		}
		document.getElementById("<portlet:namespace />birthday_sw").addEventListener("blur", function(event){
			var test = document.getElementById("<portlet:namespace />birthday_sw").value;
			if (test!=""){
			    if (isUnderAge(16)){
			    	$('#<portlet:namespace />age_disclaimerCheckbox').prop('checked', false);
			    	document.getElementById("<portlet:namespace />birthday_sw").style.color='#b50303';
			    	document.getElementById("<portlet:namespace />birthday_sw").style.borderColor = '#b50303';
			    	document.getElementById("<portlet:namespace />birthday_cl").style.color='#b50303';
			    	document.getElementById("<portlet:namespace />birthday_msg").style.display='block';
			    }
			    else{
			    	document.getElementById("<portlet:namespace />birthday_sw").style.color='green';
			    	document.getElementById("<portlet:namespace />birthday_sw").style.borderColor = 'green';
			    	document.getElementById("<portlet:namespace />birthday_cl").style.color='green';
			    	document.getElementById("<portlet:namespace />birthday_msg").style.display='none';
			    }
			}
			else{
		    	document.getElementById("<portlet:namespace />birthday_sw").style.color='#555';
		    	document.getElementById("<portlet:namespace />birthday_sw").style.borderColor = '#ccc';
		    	document.getElementById("<portlet:namespace />birthday_cl").style.color='#555';
		    	document.getElementById("<portlet:namespace />birthday_msg").style.display='none';
				
			}
		});			
		
		$(function() {
			// Remove bottom links
			$('.navigation').remove();
<%
if(sBirthday == null || ("1".equals(sBirthdayDay) && Integer.toString(Calendar.JANUARY).equals(sBirthdayMonth) && "1900".equals(sBirthdayYear))) {
%>
			// Clear birthday
			$('#<portlet:namespace />birthday_sw').val("");
<%
}
%>
		}); // end of document ready
	
		$(window).load(function() {
			$('#<portlet:namespace />fm .caret').remove();
			$('#<portlet:namespace />fm .select-dropdown').remove();

			$('#modal_novisad').appendTo($('.materialize:first'));
			
			$('#<portlet:namespace />submit').click(function(e) {
				var erroru = false;
				setDefaultBirthday();
				var test = document.getElementById("<portlet:namespace />birthday_sw").value;
				console.log("validation:"+ test);
				console.log("erroru:"+ erroru);
				if(test != ""){
					if(isUnderAge(16)) {
						erroru= true;
				    	document.getElementById("<portlet:namespace />birthday_sw").style.color='#b50303';
				    	document.getElementById("<portlet:namespace />birthday_sw").style.borderColor = '#b50303';
				    	document.getElementById("<portlet:namespace />birthday_cl").style.color='#b50303';
				    	document.getElementById("<portlet:namespace />birthday_msg").style.display='block';		
						$('#<portlet:namespace />age_disclaimerCheckbox').prop('checked', false);
					}
					else{
						erroru= false;
						console.log("validation submit:"+ document.getElementById("<portlet:namespace />birthday").value);
						var arrbirth = document.getElementById("<portlet:namespace />birthday_sw").value;
						var res = arrbirth.split("-");
						document.getElementById("<portlet:namespace />birthdayDay").value=res[2];
						var monthaux= parseInt(res[1])-1;;						
						document.getElementById("<portlet:namespace />birthdayMonth").value=monthaux;
						document.getElementById("<portlet:namespace />birthdayYear").value=res[0];
						document.getElementById("<portlet:namespace />birthday").value=res[2]+"/"+monthaux+"/"+res[0];
						console.log("validation submit:"+ document.getElementById("<portlet:namespace />birthday").value);
					}
				}
				console.log("erroru:"+ erroru);
				var pilot = $('select[name=<portlet:namespace />pilot]').val();
				<portlet:namespace />validator.validate();
				if( !<portlet:namespace />validator.hasErrors() && !erroru) {
					if(pilot === 'Novisad') {
						console.log("submit:Novisad");
						$('#modal_novisad').openModal();
					} else {
						console.log("submit:");
						submitForm(document.<portlet:namespace />fm);
					}
				}
				else{
					console.log("<portlet:namespace />validator.hasErrors()"+<portlet:namespace />validator.hasErrors());
				}
			});
		
			
			$('#modal_novisad_ok').click(function(e) {
				submitForm(document.<portlet:namespace />fm);
			});

			$('select[name=<portlet:namespace />pilot]').change(function() {
				checkLanguagePilot();
			});
			checkLanguagePilot();
			var iscompany = $('select[name=<portlet:namespace />isCompany]').val();
			if (iscompany=="true"){
				document.getElementById("showCompany").style.display='block';
			}else{
				document.getElementById("showCompany").style.display='none';
			}	
	
		}); // end of window load

	})(jQuery); // end of jQuery name space
	
	
	function showCompany(flag){
		if (flag=="true"){
			document.getElementById("showCompany").style.display='block';
		}else{
			document.getElementById("showCompany").style.display='none';
		}
	}
</script>

