
function validate_checkbox_list(val, fieldNode, ruleValue) {
	sibling_list = fieldNode.siblings();
	for(i = 0; i < sibling_list.size(); i++) { 
		node = sibling_list.item(i);
		if(node.hasClass('checkbox')) {
			child_list = node.get('children');
			for(j = 0; j < child_list.size(); j++) {
				node_child = child_list.item(j);
				if(node_child.get('type') === 'hidden') {
					if(node_child.get('value') === 'true') {
						return true;
					}
				}
			}
		}
	}
	return false;
}
