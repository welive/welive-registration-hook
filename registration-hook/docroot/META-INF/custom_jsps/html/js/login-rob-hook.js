

/*Resize dinamico del Login per estenderlo a tutta la dimensione delle finestra*/
$(function() {
		var w = $( window ).width();
		var h = $( window ).height();
		
		if($("div.portlet-login .portlet-borderless-container").length > 0){
			var loginBodyH = $(".portlet-login .portlet-borderless-container .portlet-body").height();
			$("#wrapper").css({"width": w, "height": h});
			$(".portlet-login .portlet-borderless-container .portlet-body").css({"margin-top":h/2-loginBodyH/2});
		}else{
			var loginBodyH = $(".portlet-login section .portlet-body").height();
			$("#wrapper").css({"width": w, "height": h});
			$(".portlet-login section .portlet-body").css({"margin-top":h/2-loginBodyH/2});
		}
		$("div#content").css("margin-top", "0px");
		
});
	
	$(window).resize(function(){
		
		var w = $( window ).width();
		var h = $( window ).height();
		
		if($("div.portlet-login .portlet-borderless-container").length > 0){
			var loginBodyH = $(".portlet-login .portlet-borderless-container .portlet-body").height();
			$("#wrapper").css({"width": w, "height": h});
			$(".portlet-login .portlet-borderless-container .portlet-body").css({"margin-top":h/2-loginBodyH/2});
		}else{
			var loginBodyH = $(".portlet-login section .portlet-body").height();
			$("#wrapper").css({"width": w, "height": h});
			$(".portlet-login section .portlet-body").css({"margin-top":h/2-loginBodyH/2});
		}
		$("div#content").css("margin-top", "0px");
	});