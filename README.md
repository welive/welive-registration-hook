# README #

This project is a Liferay 6.2 hook

### What is this repository for? ###

WeLive Registration 


### How do I get set up? ###

For details about configuration/installation you can see "D2.3 � WELIVE OPEN INNOVATION AND CROWDSOURCING TOOLS V1"
Remember to set the username/password for the Basic Authentication on the file \registration-hook\docroot\WEB-INF\src\portlet.properties

### Who do I talk to? ###

* ENG team, Giovanni Aiello, Filippo Giuffrida, Antonino Sirchia, Ivan Ligotino
* Tecnalia team, Eneko Gomez Ramos